package test;

import note.controller.NoteController;
import note.model.Nota;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class AdaugaTest {
	
	private NoteController ctrl;

	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

	@Before
	public void init(){
		ctrl = new NoteController();
		ctrl.readElevi("elevi.txt");
		ctrl.readNote("note.txt");
		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
	}

	@Test
	public void test1() throws ClasaException {
		ctrl.adaugaNota(10, "analiza", 10);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test2() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidMateria);
		ctrl.adaugaNota(10, "vvss", 10);
	}

	@Test
	public void test3() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		ctrl.adaugaNota(10, "analiza", -1);
	}

	@Test
	public void test4() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		ctrl.adaugaNota(Integer.MAX_VALUE, "matematica", 10);
	}

	@Test
	public void test5() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidMateria);
		ctrl.adaugaNota(1, "mate", 10);
	}


	@Test
	public void test6() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		ctrl.adaugaNota(1, "matematica", 0);
	}

	@Test
	public void test7() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidMateria);
		ctrl.adaugaNota(1, "mate", 0);
	}

	@Test
	public void test8() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		ctrl.adaugaNota(-1, "matematica", 0);
	}

	@Test
	public void test9() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidMateria);
		ctrl.adaugaNota(-1, "", 0);
	}


	@Test
	public void test10() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidMateria);
		ctrl.adaugaNota(1, "", 0);
	}
}
