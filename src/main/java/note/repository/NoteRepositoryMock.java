package note.repository;

import note.model.Elev;
import note.model.Nota;
import note.utils.ClasaException;
import note.utils.Constants;

import java.io.*;
import java.util.*;

public class NoteRepositoryMock implements NoteRepository {
    private List<Nota> note;
    private ClasaRepository clasaRepositoryMock;

    public NoteRepositoryMock(ClasaRepository clasaRepositoryMock) {
        note = new LinkedList<Nota>();
        this.clasaRepositoryMock = clasaRepositoryMock;
    }


    //	@Override
    public void addNota(Nota nota) throws ClasaException {
        // TODO Auto-generated method stub
        if (!validareNota(nota))
            throw new ClasaException(Constants.invalidNota);
//		note.add(nota);
        for (Map.Entry<Elev, HashMap<String, List<Double>>> entry : clasaRepositoryMock.getClasa().entrySet()) {
            if (entry.getKey().getNrmatricol() == nota.getNrmatricol()) {
                if (entry.getValue().get(nota.getMaterie()) == null) {
                    //entry.setValue(new HashMap<String, List<Double>>());
                    entry.getValue().put(nota.getMaterie(), new LinkedList<Double>());
                }
                entry.getValue().get(nota.getMaterie()).add(nota.getNota());
            }
        }
    }

    private boolean validareNota(Nota nota) throws ClasaException {
        // TODO Auto-generated method stub
        if (nota.getMaterie().length() < 5 || nota.getMaterie().length() > 20)
            throw new ClasaException(Constants.invalidMateria);
        if (nota.getNrmatricol() < Constants.minNrmatricol || nota.getNrmatricol() > Constants.maxNrmatricol)
            throw new ClasaException(Constants.invalidNrmatricol);
        if (nota.getNota() < Constants.minNota || nota.getNota() > Constants.maxNota)
            throw new ClasaException(Constants.invalidNota);
        if (nota.getNota() != (int) nota.getNota())
            throw new ClasaException(Constants.invalidNota);
        if (nota.getNrmatricol() != (int) nota.getNrmatricol())
            throw new ClasaException(Constants.invalidNrmatricol);
        return true;
    }

    //	@Override
    public List<Nota> getNote() {
        // TODO Auto-generated method stub
        List<Nota> note = new ArrayList<>();
        for (Map.Entry<Elev, HashMap<String, List<Double>>> entry : clasaRepositoryMock.getClasa().entrySet()) {
            for (Map.Entry<String, List<Double>> entry2 : entry.getValue().entrySet()) {
                for (double n : entry2.getValue()) {
                    note.add(new Nota(0, "", n));
                }
            }
        }
        return note;
    }

    public void readNote(String fisier) {
        try {
            FileInputStream fstream = new FileInputStream(fisier);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                Nota nota = new Nota(Double.parseDouble(values[0]), values[1], Double.parseDouble(values[2]));
                note.add(nota);
            }
            br.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


}
