package note.utils;

public class ClasaException extends Exception {
    public ClasaException() {
        super();
    }

    public ClasaException(String message) {
        super(message);
    }

    public ClasaException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClasaException(Throwable cause) {
        super(cause);
    }

    protected ClasaException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
